//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MSP430_IMMEDIATE_H
#define MSP430EMU_MSP430_IMMEDIATE_H

#include "../../base/interfaces/AOperand.h"
#include "../../base/exceptions/exceptions.h"

namespace msp430 {
    class Immediate : public vmc::AOperand {
    public:
        const uint32_t data;

        Immediate(uint32_t vdata, Size vsize) : data(vdata), AOperand(IMMEDIATE, vsize) {}

        // TODO: Add 16-bit mask on value
        uint32_t value(vmc::PDevice dev) const override {
            return data;
        }

        // TODO: Add 16-bit mask on value
        void value(vmc::PDevice dev, uint32_t data) const override {
            throw new exc::UnsupportOperationException();
        }

        std::string stringify() const override {
            return utils::sformat("#%04X", data);
        }
    };
}

#endif //MSP430EMU_MSP430_IMMEDIATE_H
