//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MSP430_CPU_H
#define MSP430EMU_MSP430_CPU_H

#include "../../base/interfaces/ACPU.h"
#include "../../base/interfaces/ADevice.h"

namespace msp430 {
    enum STATUS_REGISTER_BITS {
        STATUS_C = 0,
        STATUS_Z = 1,
        STATUS_N = 2,
        STATUS_GIE = 3,
        STATUS_CPUOFF = 4,
        STATUS_OSCOFF = 5,
        STATUS_SG0 = 6,
        STATUS_SG1 = 7,
        STATUS_V = 8
    };

    class CPU : public vmc::ACPU {
    public:
        CPU() {
            _regs.resize(16);
        }
    };
}


#endif //MSP430EMU_MSP430_CPU_H
