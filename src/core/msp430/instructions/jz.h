//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_JZ_H
#define MSP430EMU_JZ_H

#include "../../base/interfaces/AInstruction.h"
#include "../operands/Void.h"
#include "../operands/Register.h"
#include "../hardware/CPU.h"

namespace msp430 {
    class Jz : public vmc::AInstruction {
    public:
        Jz(const vmc::PAOperand vop1) : AInstruction("jz", vop1, std::make_shared<Void>(), 2) { }

        bool execute(vmc::PDevice dev) override {
            auto z = Register::SR->bit(dev, STATUS_Z);
            if (z!=0) {
                auto pc = Register::PC->value(dev);
                pc += op1->value(dev);
                Register::PC->value(dev, pc);
            }
            return true;
        }
    };
}

#endif //MSP430EMU_JZ_H
