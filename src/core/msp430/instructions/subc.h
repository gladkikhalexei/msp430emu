//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_SUBC_H
#define MSP430EMU_SUBC_H

#include "../../base/interfaces/AInstruction.h"
#include "../operands/Register.h"
#include "../hardware/CPU.h"

namespace msp430 {
    class Subc : public vmc::AInstruction {
    public:
        Subc(const vmc::PAOperand vop1, const vmc::PAOperand vop2, const int size) :
                AInstruction("subc", vop1, vop2, size) { }

        bool execute(vmc::PDevice dev) override {
            auto src = op1->value(dev);
            auto dst = op2->value(dev);
            auto c = Register::SR->bit(dev, STATUS_C);
            auto res = dst - src + c;
            op2->value(dev, res);
            if (op1->size == vmc::AOperand::BYTE) { //set overflow flag
                if (((~(dst-1)&src&res) >> 7)||(((dst-1)&~src&~res) >> 7)) {
                    Register::SR->bit(dev, STATUS_V, 1);
                } else Register::SR->bit(dev, STATUS_V, 0);
                if ((((src-1) & ~dst & res) >> 7) || ((~(src-1) & dst & res) >> 7)) {
                    Register::SR->bit(dev, STATUS_C, 1);
                } else Register::SR->bit(dev, STATUS_C, 0);
                if (res >> 7) {
                    Register::SR->bit(dev, STATUS_N, 1);
                } else Register::SR->bit(dev, STATUS_N, 0);
                if (res==0x00)
                    Register::SR->bit(dev, STATUS_Z, 1);
            }
            if (op1->size == vmc::AOperand::WORD) { //set overflow flag
                if (((~(dst-1)&src&res) >> 15)||(((dst-1)&~src&~res) >> 15)) {
                    Register::SR->bit(dev, STATUS_V, 1);
                } else Register::SR->bit(dev, STATUS_V, 0);
                if ((((src-1) & ~dst & res) >> 15) || ((~(src-1) & dst & res) >> 15)) {
                    Register::SR->bit(dev, STATUS_C, 1);
                } else Register::SR->bit(dev, STATUS_C, 0);
                if (res >> 15) {
                    Register::SR->bit(dev, STATUS_N, 1);
                } else Register::SR->bit(dev, STATUS_N, 0);
                if (res==0x0000)
                    Register::SR->bit(dev, STATUS_Z, 1);
            }
            return true;
        }
    };
}

#endif //MSP430EMU_ADD_H
