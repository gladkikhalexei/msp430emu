//
// Created by ALG on 01.11.2016.
//

#ifndef MSP430EMU_SXT_H
#define MSP430EMU_SXT_H
#include "../../base/interfaces/ADevice.h"
#include "../operands/Void.h"
#include "../operands/Register.h"
#include "../hardware/CPU.h"

namespace msp430 {
    class Sxt : public vmc::AInstruction {
    public:
        Sxt(const vmc::PAOperand vop1) :
                AInstruction("sxt", vop1, std::make_shared<Void>(), 2) { }

        bool execute(vmc::PDevice dev) override {
            auto dst = op1->value(dev);
            auto lsb = op1->bit(dev, 7);
            if (lsb)
            {
                dst|=0xFF00;
                            }
            else dst&=0x00FF;
            op1->value(dev, dst);
            auto sign = op1->bit(dev, 15);
            if (sign)
                Register::SR->bit(dev, STATUS_N, 1);
            else Register::SR->bit(dev, STATUS_N, 0);
            if (dst==0x0000)
            {
                Register::SR->bit(dev, STATUS_Z, 1);
                Register::SR->bit(dev, STATUS_C, 0);
            }
            else
            {
                Register::SR->bit(dev, STATUS_Z, 0);
                Register::SR->bit(dev, STATUS_C, 1);
            }
            Register::SR->bit(dev, STATUS_V, 0);
            return true;
        }
    };
}
#endif //MSP430EMU_SXT_H
