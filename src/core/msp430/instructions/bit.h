//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_BIT_H
#define MSP430EMU_BIT_H

#include "../../base/interfaces/AInstruction.h"
#include "../operands/Register.h"
#include "../hardware/CPU.h"

namespace msp430 {
    class Bit : public vmc::AInstruction {
    public:
        Bit(const vmc::PAOperand vop1, const vmc::PAOperand vop2, const int size) :
                AInstruction("bit", vop1, vop2, size) { }

        bool execute(vmc::PDevice dev) override {
            auto src = this->op1->value(dev);
            auto dst = this->op2->value(dev);
            auto res = src & dst;
            Register::SR->bit(dev, STATUS_V, 0);//always 0 for add
            if (op1->size == vmc::AOperand::BYTE) { //byte mode
                if (res >> 7) {
                    Register::SR->bit(dev, STATUS_N, 1);
                } else Register::SR->bit(dev, STATUS_N, 0);
                if (res==0x00) {
                    Register::SR->bit(dev, STATUS_Z, 1);
                    Register::SR->bit(dev, STATUS_C, 0);
                } else Register::SR->bit(dev, STATUS_C, 1);// c=1 if res!=0
            }
            if (op1->size == vmc::AOperand::WORD) { //word mode
                if (res >> 15) {
                    Register::SR->bit(dev, STATUS_N, 1);
                } else Register::SR->bit(dev, STATUS_N, 0);
                if (res==0x0000) {
                    Register::SR->bit(dev, STATUS_Z, 1);
                    Register::SR->bit(dev, STATUS_C, 0);
                } else Register::SR->bit(dev, STATUS_C, 1);// c=1 if res!=0
            }
            return true;
        }
    };
}

#endif //MSP430EMU_BIT_H
