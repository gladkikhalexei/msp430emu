//
// Created by ALG on 01.11.2016.
//

#ifndef MSP430EMU_SWPB_H
#define MSP430EMU_SWPB_H
#include "../../base/interfaces/ADevice.h"
#include "../operands/Void.h"
#include "../operands/Register.h"

namespace msp430 {
    class Swpb : public vmc::AInstruction {
    public:
        Swpb(const vmc::PAOperand vop1) :
                AInstruction("swpb", vop1, std::make_shared<Void>(), 2) { }

        bool execute(vmc::PDevice dev) override {
            auto dst = this->op1->value(dev);
            auto buffer = (dst&0x00FF)<<7;
            buffer|=(dst&0xFF00)>>7;
            this->op1->value(dev, buffer);
            return true;
        }
    };
}
#endif //MSP430EMU_SWPB_H
