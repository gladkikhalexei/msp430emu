//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_BIC_H
#define MSP430EMU_BIC_H

#include "../../base/interfaces/AInstruction.h"

namespace msp430 {
    class Bic : public vmc::AInstruction {
    public:
        Bic(const vmc::PAOperand vop1, const vmc::PAOperand vop2, const int size) :
                AInstruction("bic", vop1, vop2, size) { }

        bool execute(vmc::PDevice dev) override {
            auto src = this->op1->value(dev);
            auto dst = this->op2->value(dev);
            dst &= ~src;
            this->op2->value(dev, dst);
            return true;
        }
    };
}

#endif //MSP430EMU_BIC_H
