//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_DUMMY_VOID_H
#define MSP430EMU_DUMMY_VOID_H

#include "../../base/interfaces/AOperand.h"

namespace dummy {
    class Void : public vmc::AOperand {

    };
}

#endif //MSP430EMU_VOID_H
