//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_SYSTEMDECODER_H
#define MSP430EMU_SYSTEMDECODER_H

#include "../../base/interfaces/ADecoder.h"
#include "../instructions/mov.h"
#include "../operands/Register.h"

namespace dummy {
    class SystemDecoder : public vmc::ADecoder {
    public:
        vmc::PAInstruction decode(std::vector<uint8_t> data) override {
            auto op1 = std::make_shared<Register>();
            auto op2 = std::make_shared<Register>();
            return std::make_shared<mov>(op1, op2, 4);
        }
    };
}

#endif //MSP430EMU_SYSTEMDECODER_H
