//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_DUMMY_DEVICE_H
#define MSP430EMU_DUMMY_DEVICE_H

#include "../base/interfaces/ADevice.h"
#include "../base/Breakpoints.h"
#include "hardware/CPU.h"
#include "hardware/SystemMemory.h"
#include "hardware/SystemDecoder.h"
#include "hardware/SystemTimer.h"

namespace dummy {
    class Device : public vmc::ADevice {
    public:
        Device() {
            cpu = std::make_shared<CPU>();
            memory = std::make_shared<SystemMemory>();
            decoder = std::make_shared<SystemDecoder>();
            timer = std::make_shared<SystemTimer>();
        }
    };
}
#endif //MSP430EMU_DEVICE_H
