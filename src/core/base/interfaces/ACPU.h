//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_ICPU_H
#define MSP430EMU_ICPU_H

#include <cstdint>
#include <memory>
#include <vector>
#include "ADeviceComponent.h"

namespace vmc {
    class ACPU : public ADeviceComponent {
    protected:
        std::vector<uint32_t> _regs;
    public:
        uint32_t pc = 0;

        uint32_t reg(int indx) {
            if (indx == 0)
                return pc;
            return _regs[indx];
        }
        void reg(int indx, uint32_t value) {
            if (indx == 0)
                pc = value;
            else
                _regs[indx] = value;
        }
    };

    typedef std::shared_ptr<ACPU> PACPU;
}

#endif //MSP430EMU_ICPU_H
