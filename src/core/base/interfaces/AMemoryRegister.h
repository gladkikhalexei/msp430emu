//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MEMORY_REGISTER_H
#define MSP430EMU_MEMORY_REGISTER_H

#include <cstdint>
#include <memory>

namespace vmc {
    class AMemoryRegister {
    public:
        virtual uint32_t onRead(uint32_t vAddr, int size) = 0;
        virtual void onWrite(uint32_t vAddr, uint32_t data, int size) = 0;
        virtual bool isAccessed(uint32_t vAddr, int size) = 0;
    };

    typedef std::shared_ptr<AMemoryRegister> PAMemoryRegister;
}

#endif //MSP430EMU_MEMORY_REGISTER_H
