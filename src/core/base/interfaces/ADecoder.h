//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_DECODER_H
#define MSP430EMU_DECODER_H

namespace vmc {
    class AInstruction;
    typedef std::shared_ptr<AInstruction> PAInstruction;

    class ADecoder : public ADeviceComponent {
    public:
        virtual PAInstruction decode(std::vector<uint8_t> data) = 0;
    };

    typedef std::shared_ptr<ADecoder> PDecoder;
}

#endif //MSP430EMU_DECODER_H
