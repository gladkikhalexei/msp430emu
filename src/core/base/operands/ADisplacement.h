//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_ADISPLACEMENT_H
#define MSP430EMU_ADISPLACEMENT_H

#include "../interfaces/AOperand.h"

namespace vmc {
    class ADisplacement : public AOperand {
        ADisplacement(Size vsize) : AOperand(DISPLACEMENT, vsize) { };
    };
}

#endif //MSP430EMU_ADISPLACEMENT_H
